package com.cappton.clubes.controller;

import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.cappton.clubes.model.*;
import com.cappton.clubes.repository.*;

@RestController
@RequestMapping({ "/clubes" })
public class MainController {

	@Autowired
	private DaoClub daoclub;

	@Autowired
	private DaoJugador daojugador;

	// POST Alta de club
	@CrossOrigin(origins = "http://localhost:4200")
	@PostMapping(path = { "/club" })
	public Club create(@RequestBody Club club) {

		return daoclub.save(club);
	}

	// POST para dar de alta al jugador
	@CrossOrigin(origins = "http://localhost:4200")
	@PostMapping(path = { "/jugador" })
	public ResponseEntity<Object> createJugador(@RequestBody InputJugador jugador) {

		// busco el usuario por id
		Club cl1 = daoclub.findById(jugador.getIdClub()).orElse(null);

		// creo un obj jugador
		Jugador jug = new Jugador();

		if (cl1 != null) {

			jug.setClub(cl1);
			jug.setNombre(jugador.getJugador().getNombre());
			jug.setEdad(jugador.getJugador().getEdad());
			jug.setFoto(jugador.getJugador().getFoto());

			Jugador jugadore = daojugador.save(jug);

			// me hago un obj que diga lo del error
			JSONObject obj = new JSONObject();
			obj.put("error", 0);
			obj.put("message", jugadore.getId()); // aca devuelvo el id de base de datos del jugador

			return ResponseEntity.ok().body(obj.toString());

		} else {

			JSONObject obj = new JSONObject();
			obj.put("error", 1);
			obj.put("message", "User not found"); // aca muestro un mensaje diciendo que el club no se encontro

			return ResponseEntity.ok().body(obj.toString());
		}

	}

	// GET Traer todos los jugadores
	@CrossOrigin(origins = "http://localhost:4200")
	@GetMapping(path = { "/clubes/{idclub}" })
	public ResponseEntity<Object> getJugadoresClub(@PathVariable Long idclub) {
		Club c1 = daoclub.findById(idclub).orElse(null);
		List<Jugador> jugadoresFound = daojugador.findByClub(c1);

		// creo un array que lo van a usar todos
		JSONArray json_array = new JSONArray();

		if (jugadoresFound.size() > 0) {

			// si encontro
			for (Jugador jug : jugadoresFound) {

				// armo el Json 1 obj nuevo por cada registro
				JSONObject aux = new JSONObject();
				aux.put("nombre", jug.getNombre());
				aux.put("edad", jug.getEdad());
				aux.put("foto", jug.getFoto());
				json_array.put(aux);

			}
		}

		JSONObject obj = new JSONObject();
		obj.put("error", 0);
		obj.put("results", json_array);
		obj.put("clubNombre", c1.getNombre());
		obj.put("clubPass", c1.getPass());
		obj.put("clubLogo", c1.getLogo());

		return ResponseEntity.ok().body(obj.toString());
	}

	// GET para traer los clubes
	@CrossOrigin(origins = "http://localhost:4200")
	@GetMapping(path = { "/login" })
	public ResponseEntity<Object> getClubes() {

		List<Club> clubFound = daoclub.findAll();

		// creo un array que lo van a usar todos
		JSONArray json_array = new JSONArray();

		if (clubFound.size() > 0) {

			// si encontro
			for (Club cb : clubFound) {

				// armo el Json 1 obj nuevo por cada registro
				JSONObject aux = new JSONObject();
				aux.put("id", cb.getId());
				aux.put("nombre", cb.getNombre());
				aux.put("pass", cb.getPass());
				aux.put("logo", cb.getLogo());
				json_array.put(aux);

			}
		}

		JSONObject obj = new JSONObject();
		obj.put("error", 0);
		obj.put("results", json_array);

		return ResponseEntity.ok().body(obj.toString());
	}

	// POST para Mod jugador
	@CrossOrigin(origins = "http://localhost:4200")
	@PostMapping(path = { "/fichaje" })
	public ResponseEntity<Object> fichaJugador(Long idjug, Long idClub) {

		// busco el usuario por id
		Club cl1 = daoclub.findById(idClub).orElse(null);
		// busco el id del post
		Jugador jug = daojugador.findById(idjug).orElse(null);

		if (cl1 != null && jug != null) {

			// Mod club jugador
			jug.setClub(cl1);
			daojugador.save(jug);
			// me hago un obj que diga lo del error
			JSONObject obj = new JSONObject();
			obj.put("error", 0);
			obj.put("message", "Jugador mod con exito");

			return ResponseEntity.ok().body(obj.toString());

		} else {

			JSONObject obj = new JSONObject();
			obj.put("error", 1);
			obj.put("message", "User/Post not found");

			return ResponseEntity.ok().body(obj.toString());
		}

	}

}
