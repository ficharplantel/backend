package com.cappton.clubes.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.cappton.clubes.model.Club;
import com.cappton.clubes.model.Jugador;

public interface DaoJugador extends JpaRepository<Jugador, Long> {
	
	public List <Jugador> findByClub(Club club);

}
