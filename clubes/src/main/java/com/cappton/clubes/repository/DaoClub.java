package com.cappton.clubes.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.cappton.clubes.model.Club;

public interface DaoClub extends JpaRepository<Club, Long> {

}
