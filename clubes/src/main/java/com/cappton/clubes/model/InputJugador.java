package com.cappton.clubes.model;

public class InputJugador {
	
	private Long idClub;
	private Jugador jugador;
	
	public Long getIdClub() {
		return idClub;
	}
	public void setIdClub(Long idClub) {
		this.idClub = idClub;
	}
	public Jugador getJugador() {
		return jugador;
	}
	public void setJugador(Jugador jugador) {
		this.jugador = jugador;
	}
	

}
